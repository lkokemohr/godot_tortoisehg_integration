tool
extends Control

func _on_SetupButton_pressed():
	OS.execute("thg", ["init"], true)

func _on_CommitButton_pressed():
	OS.execute("thg", ["commit"], true)

func _on_PushButton_pressed():
	OS.execute("thg", ["sync"], true)

func _on_WorkbenchButton_pressed():
	OS.execute("thg", [], true)
