# README #

This is a plugin for Godot which creates a new panel with shortcuts to the most important TortoiseHg windows.
The first button ("Create local repo") creates a local repository so that the project folder of the current project becomes a working copy.